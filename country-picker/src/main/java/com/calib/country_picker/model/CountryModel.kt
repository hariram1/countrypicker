package com.calib.country_picker.model

data class CountryModel(
    val countryCode: String? = null, val countryName: String? = null,
    val countryDialCode: String? = null, val countryFlag: Int = 0,
    val currency: String? = null
)