package com.calib.country_picker.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.calib.country_picker.databinding.ListItemCountryBinding
import com.calib.country_picker.model.CountryModel
import com.calib.country_picker.CountryPicker

class CountryCodePickerAdapter(
    private var countryList: ArrayList<CountryModel>,
    private var countryPickerListener: CountryPicker.CountryCodePickerListener
) : RecyclerView.Adapter<CountryCodePickerAdapter.MainViewHolder>() {

    inner class MainViewHolder(val binding: ListItemCountryBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        return MainViewHolder(
            ListItemCountryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return countryList.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        with(holder) {
            with(countryList) {
                binding.flagListItemCountry.setImageResource(this[position].countryFlag)
                binding.countryNameTvListItemCountry.text = this[position].countryName
                binding.countryDialCodeListItemCountry.text = this[position].countryDialCode
            }
            itemView.setOnClickListener {
                countryPickerListener.onCountrySelected(countryList[position])
            }
        }
    }
}