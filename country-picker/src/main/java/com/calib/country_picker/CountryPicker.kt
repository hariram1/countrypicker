package com.calib.country_picker

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.calib.country_picker.adapter.CountryCodePickerAdapter
import com.calib.country_picker.databinding.BsCountryPickerBinding
import com.calib.country_picker.model.CountryModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.util.*

class CountryPicker(private val context: Context, private val listener: CountryCodePickerListener) :
    BottomSheetDialog(context) {

    private lateinit var binding: BsCountryPickerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = BsCountryPickerBinding.inflate(layoutInflater)

        val contentView: View = binding.root
        setContentView(contentView)
        (contentView.parent as View).setBackgroundColor(
            ResourcesCompat.getColor(
                context.resources,
                android.R.color.transparent,
                context.theme
            )
        )

        countries = getCountries()
        searchResults.addAll(countries)

        adapter = CountryCodePickerAdapter(searchResults, listener)

        binding.countriesRecyclerCountryPicker.layoutManager = LinearLayoutManager(context)
        binding.countriesRecyclerCountryPicker.adapter = adapter

        binding.searchEtCountryPicker.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(searchQuery: Editable) {
                search(searchQuery.toString())
            }
        })
        binding.searchEtCountryPicker.setOnEditorActionListener { _, _, _ ->
            val imm: InputMethodManager = binding.searchEtCountryPicker.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(binding.searchEtCountryPicker.windowToken, 0)
            true
        }
    }

    private fun loadJSONFromAsset(): String? {
        val json: String? = try {
            val input: InputStream = context.assets.open(context.getString(R.string.countries_json))
            val size: Int = input.available()
            val buffer = ByteArray(size)
            input.read(buffer)
            input.close()
            String(buffer, Charsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    private fun getCountries(): ArrayList<CountryModel> {
        val countries = arrayListOf<CountryModel>()
        try {
            val jsonString = loadJSONFromAsset()
            jsonString?.let {
                val obj = JSONObject(jsonString)
                val jsonArray = obj.getJSONArray(context.getString(R.string.countries))
                for (i in 0 until jsonArray.length()) {
                    val countryObject = jsonArray.getJSONObject(i)

                    val countryCode = countryObject.getString(context.getString(R.string.countrycode))
                    val countryName = countryObject.getString(context.getString(R.string.countryname))
                    val countryDialCode = countryObject.getString(context.getString(R.string.countrydialcode))
                    val countryFlag = countryObject.getString(context.getString(R.string.countryflag))
                    val currency = countryObject.getString(context.getString(R.string.currency))

                    val countryModel = CountryModel(
                        countryCode, countryName, countryDialCode,
                        R.drawable::class.java.getField(countryFlag).getInt(null), currency
                    )
                    countries.add(countryModel)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return countries
    }

    companion object {

        var countries: ArrayList<CountryModel> = arrayListOf()
        var searchResults: ArrayList<CountryModel> = arrayListOf()
        lateinit var adapter: CountryCodePickerAdapter

        // Country Dial Code Picker
        private const val SORT_BY_NONE: Int = 0
        private const val SORT_BY_NAME: Int = 1
        private const val SORT_BY_ISO: Int = 2
        private const val SORT_BY_DIAL_CODE: Int = 3

        private var sortType: Int = SORT_BY_NONE

        fun updateSortType(sortBy: Int) {
            sortType = sortBy
        }

        private fun sortCountries() {
            when (sortType) {
                SORT_BY_NONE -> {}
                SORT_BY_NAME -> {
                    searchResults.sortWith { country1, country2 ->
                        country1.countryName!!.trim().compareTo(country2.countryName!!.trim(), true)
                    }
                }

                SORT_BY_ISO -> {
                    searchResults.sortWith { country1, country2 ->
                        country1.countryCode!!.trim().compareTo(country2.countryCode!!.trim(), true)
                    }
                }

                SORT_BY_DIAL_CODE -> {
                    searchResults.sortWith { country1, country2 ->
                        country1.countryDialCode!!.trim()
                            .compareTo(country2.countryDialCode!!.trim(), true)
                    }
                }
            }
        }

        private fun search(searchQuery: String) {
            searchResults.clear()
            for (country in countries) {
                if (country.countryName!!.lowercase(Locale.ENGLISH)
                        .contains(searchQuery.lowercase(Locale.getDefault())) ||
                    country.countryCode!!.lowercase(Locale.ENGLISH)
                        .contains(searchQuery.lowercase(Locale.getDefault())) ||
                    country.countryDialCode!!.lowercase(Locale.ENGLISH)
                        .contains(searchQuery.lowercase(Locale.getDefault()))
                ) {
                    searchResults.add(country)
                }
            }
            sortCountries()
            adapter.notifyDataSetChanged()
        }
    }

    interface CountryCodePickerListener {
        fun onCountrySelected(countryModel: CountryModel)
    }
}
