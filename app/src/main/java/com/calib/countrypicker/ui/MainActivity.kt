package com.calib.countrypicker.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.calib.country_picker.CountryPicker
import com.calib.country_picker.model.CountryModel
import com.calib.countrypicker.databinding.MainActivityBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity: AppCompatActivity(), CountryPicker.CountryCodePickerListener {

    private lateinit var mainActivityBinding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainActivityBinding = MainActivityBinding.inflate(layoutInflater)
        setContentView(mainActivityBinding.root)

        val countryPicker = CountryPicker(this, this)
        countryPicker.show()
    }

    override fun onCountrySelected(countryModel: CountryModel) {
        Snackbar.make(mainActivityBinding.root, countryModel.countryName + "", Snackbar.LENGTH_LONG).show()
    }
}